THUMB_PATH="./static/img/thumbnail/$1"
mkdir $THUMB_PATH
mogrify -path $THUMB_PATH -format jpg -define jpeg:size=200x200 -thumbnail 200x200^ -gravity center -extent 200x200 -quality 85 "./static/img/large/$1/*.jpg"
